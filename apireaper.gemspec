#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'apireaper/version'

dev_deps = {
  'bundler' => '~> 1.12',
  'rspec' => '~> 3.5',
  'rake' => '~> 11.2',
  'rubocop' => '~> 0.41',
  'webmock' => '~> 2.0.2',
  'simplecov' => '~> 0.12'
}

deps = {
  'json-schema' => '~> 1.2',
  'thor' => '~> 0.19'
}

Gem::Specification.new do |s|
  s.name = 'apireaper'
  s.version = APIReaper::VERSION
  s.authors = ['Richard Delaplace']
  s.email = 'rdelaplace@vente-privee.com'
  s.license = 'Apache-2.0'

  s.summary = 'Tool to request and check an API.'
  s.description = 'APIReaper is a simple tool to assess an API and its answer.'
  s.homepage = 'https://github.com/vp-noc/apireaper'

  s.files = `git ls-files`.lines.map(&:chomp)
  s.bindir = 'bin'
  s.executables = `git ls-files bin/*`.lines.map do |exe|
    File.basename(exe.chomp)
  end
  s.require_paths = ['lib']

  s.required_ruby_version = '>= 1.9.3'

  dev_deps.each_pair do |deps_gem, deps_version|
    s.add_development_dependency deps_gem, deps_version
  end

  deps.each_pair do |deps_gem, deps_version|
    s.add_dependency deps_gem, deps_version
  end
end
