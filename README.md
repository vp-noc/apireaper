# APIReaper
[![License](https://img.shields.io/badge/license-Apache-blue.svg)](LICENSE)
[![build status](https://gitlab.com/vp-noc/apireaper/badges/develop/build.svg)](https://gitlab.com/vp-noc/apireaper/commits/develop)
[![coverage report](https://gitlab.com/vp-noc/apireaper/badges/develop/coverage.svg)](https://vp-noc.gitlab.io/apireaper/)
[![Gem Version](https://badge.fury.io/rb/apireaper.svg)](https://badge.fury.io/rb/apireaper)

1. [Overview](#overview)
2. [Description](#role-description)
3. [Setup](#setup)
4. [Usage](#usage)
5. [Limitations](#limitations)
6. [Development](#development)
7. [Miscellaneous](#miscellaneous)

## Overview

`APIReaper` is a simple tool to assess an API and its answer.

## Description

This tool validate if an API is able to reply to an incoming request and check
if the data structure sent back is valid. It requires a valid schema to compare
the data against.

## Setup

    $ gem install apireaper

## Usage

An interactive help is available with:

    $ apireaper help

## Examples

To check an API:

    $ apireaper check POST "<API_URL>" -h "<HEADER_KEY>:<HEADER_VALUE>" \
     -d "<DATA_KEY>=<DATA_VALUE>" -S '<JSON_SCHEMA_OBJECT_AS_STRING>'

To check an API with a schema in a file:

    $ apireaper check POST "<API_URL>" -h "<HEADER_KEY>:<HEADER_VALUE>" \
     -d "<DATA_KEY>=<DATA_VALUE>" -F '<PATH_TO_JSON_SCHEMA_FILE>'

## Limitations

It is currently only available to validate JSON data structure.
More data structure may be available later.

## Development

Please read carefully [CONTRIBUTING.md](CONTRIBUTING.md) before making a merge
request.

```
    ╚⊙ ⊙╝
  ╚═(███)═╝
 ╚═(███)═╝
╚═(███)═╝
 ╚═(███)═╝
  ╚═(███)═╝
   ╚═(███)═╝
```
