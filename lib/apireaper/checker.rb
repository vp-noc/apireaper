#!/usr/bin/ruby
#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'net/http'
require 'uri'
require 'json'
require 'json-schema'

module APIReaper
  # This is an API response and data structure checker.
  class Checker
    # Class constructor method
    def initialize(method, url, opts)
      @requester = APIReaper::Requester.new(method, url, opts)
      @opts = opts
    end

    # Main method to run the check
    def check
      res = @requester.request
      check_response_code(res.code)
      check_data_structure(res.body)
      File.write(@opts['outfile'], res.body) unless @opts['outfile'].nil?
      puts 'All checks passed' unless @opts['quiet']
    end

    # Check data structure
    def check_data_structure(data)
      JSON::Validator.validate!(find_schema_source, data)
      puts 'Response body is valid' unless @opts['quiet']
    rescue JSON::Schema::ValidationError => e
      exit_with_error(2, e.message)
    end

    # Check respose code
    def check_response_code(code)
      if code.to_i.between?(200, 299)
        puts "Response code is valid: #{code}" unless @opts['quiet']
      else
        exit_with_error(2, "Response code is invalid: #{code}")
      end
    end

    # Exit with the specified errno and message
    def exit_with_error(errno, message)
      puts message unless @opts['quiet']
      exit errno
    end

    # Get the data structure schema source
    def find_schema_source
      if @opts['schema_file'].nil?
        JSON.parse(
          @opts['schema'].gsub(/:([a-zA-z]+)/, '"\\1"').gsub('=>', ': ')
        )
      else
        @opts['schema_file']
      end
    end
  end
end
