#!/usr/bin/ruby
#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'net/http'
require 'cgi'
require 'json'

module APIReaper
  # This is a Net HTTP requester class.
  class Requester
    # Class constructor method
    def initialize(method, url, opts)
      @method = method
      @uri = URI.parse(url)
      @opts = opts
      @http = Net::HTTP.new(@uri.host, @uri.port)
      @http.use_ssl = @uri.scheme.eql?('https')
      @http.verify_mode = OpenSSL::SSL::VERIFY_NONE if opts['insecure']
    end

    # Exit with the specified errno and message
    def exit_with_error(errno, message)
      puts message unless @opts['quiet']
      exit errno
    end

    # Set the authentication data
    def basic_auth(request)
      unless @opts['user'].nil? || @opts['password'].nil?
        request.basic_auth(@opts['user'], @opts['password'])
      end
      request
    end

    # Select the right data type
    def format_data(request)
      return request if @opts['data'].nil?
      if json?(@opts['data']) then format_json_data(request)
      else format_www_form_data(request)
      end
    end

    # Set the headers given as argument
    def format_header(request)
      unless @opts['header'].nil?
        @opts['header'].split(',').each do |h|
          (key, value) = h.split(':')
          request[key] = value
        end
      end
      request
    end

    # Set json data as request body
    def format_json_data(request)
      request.body = @opts['data']
      request
    end

    # Split the data string into a hash
    def format_www_form_data(request)
      request.set_form_data(CGI.parse(@opts['data']))
      request
    end

    # Check if the data string is a well formated json
    def json?(str)
      JSON.parse(str)
      true
    rescue JSON::ParserError
      false
    end

    # DELETE method
    def rest_request_delete
      exit_with_error(3, 'DELETE is not yet implemented')
      # TODO
    end

    # GET method
    def rest_request_get
      req = Net::HTTP::Get.new(@uri.request_uri)
      req = format_header(req)
      req = basic_auth(req)
      @http.request(req)
    end

    # POST method
    def rest_request_post
      req = Net::HTTP::Post.new(@uri.request_uri)
      req = format_data(req)
      req = format_header(req)
      req = basic_auth(req)
      @http.request(req)
    end

    # PUT method
    def rest_request_put
      exit_with_error(3, 'PUT is not yet implemented')
      # TODO
    end

    # Select request method
    def request
      case @method.upcase
      when 'DELETE' then rest_request_delete
      when 'GET' then rest_request_get
      when 'POST' then rest_request_post
      when 'PUT' then rest_request_put
      else raise "#{@method.upcase} is an unknown method"
      end
    end
  end
end
