#!/usr/bin/ruby
#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'thor'

module APIReaper
  # Simple CLI for apireaper
  class CLI < Thor
    desc 'version', 'Print apireaper current version'
    def version
      puts "APIReaper version #{APIReaper::VERSION}"
    end

    desc('check METHOD URL [options]',
         'Send a request to an API and catch the reply in order to validate '\
         'the data structure.')
    option(
      :header,
      aliases: ['-h'],
      desc: 'Http headers used with the request (comma separated). ' \
            '<key>:<value>[,<key>:<value>[,...]]'
    )
    option(
      :user,
      aliases: ['-u'],
      desc: 'User name in order to login. See also password option.'
    )
    option(
      :password,
      aliases: ['-p'],
      desc: 'User password in order to login. See also user option.'
    )
    option(
      :data,
      aliases: ['-d'],
      desc: 'Set of data sent with the request. ' \
            'May be of type www_form or json.' \
            'If no specific data type is detected, www_form is used.'
    )
    option(
      :schema,
      aliases: ['-S'],
      default: '{"type"=>"object"}',
      desc: 'Data structure description as formated schema'
    )
    option(
      :schema_file,
      aliases: ['-F'],
      desc: 'Data structure description as formated schema in local file'
    )
    option(
      :outfile,
      aliases: ['-o'],
      desc: 'Write the response body to output file.'
    )
    option(
      :insecure,
      aliases: ['-k'],
      type: :boolean,
      default: false,
      desc: 'Do not verify certificates using SSL'
    )
    option(
      :quiet,
      aliases: ['-q'],
      type: :boolean,
      default: false,
      desc: 'Silently do the job'
    )
    option(
      :simulation,
      aliases: ['-s'],
      type: :boolean,
      default: false,
      desc: 'Simulation mode. Do nothing'
    )
    def check(method, url)
      opts = options.dup
      checker = APIReaper::Checker.new(method, url, opts)
      checker.check unless opts['simulation']
    end
  end
end
