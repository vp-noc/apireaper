#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

def print_out(code)
  case code
  when 200, 201, 202
    "Response code is valid: #{code}\n" \
    "Response body is valid\n" \
    "All checks passed\n"
  end
end

describe APIReaper::Checker do # rubocop:disable Metrics/BlockLength
  context 'check post http://api.test.yueyehua.net/ -h k:v -d k=v' do
    it 'requests an API and test the response body without defined schema.' do
      expect { start(self) }.to output(print_out(204)).to_stdout
    end
  end

  schema_file = 'spec/files/schema'
  opts204r1 = "-h k:v -d k=v -F #{schema_file}"
  context "check post http://api.test.yueyehua.net/ #{opts204r1}" do
    it 'requests an API and test the response body with schema file.' do
      expect { start(self) }.to output(print_out(204)).to_stdout
    end
  end

  schema = File.read(schema_file)
  opts204r2 = "-h k:v -d k=v -S #{schema}"
  context "check post http://api.test.yueyehua.net/ #{opts204r2}" do
    it 'requests an API and test the response body with a schema as option.' do
      expect { start(self) }.to output(print_out(204)).to_stdout
    end
  end

  body = 'spec/files/body'
  test = 'test-outfile'
  out204r3 = "-h k:v -d k=v -o #{test}"
  context "check post http://api.test.yueyehua.net/ #{out204r3}" do
    it 'requests an API and test the response body and save it to file.' do
      expect { start(self) }.to output(print_out(204)).to_stdout
      expect { print File.read(test) }.to output(File.read(body)).to_stdout
      File.delete(test)
    end
  end

  schema_file = 'spec/files/schema'
  opts205 = "-h k:v -d k=v -q -F #{schema_file}"
  context "check post http://api.fail.yueyehua.net/ #{opts205}" do
    it 'requests an API and test the response body with wrong schema file.' do
      expect { start(self) }.to raise_error(SystemExit)
    end
  end

  context 'check post http://api.fail.yueyehua.net/ -h l:w -d l=w -q' do
    it 'requests an API and receive 404 status code.' do
      expect { start(self) }.to raise_error(SystemExit)
    end
  end

  context 'check post http://api.test.yueyehua.net/ -q' do
    it 'requests an API with quiet option.' do
      expect { start(self) }.to output('').to_stdout
    end
  end
end
