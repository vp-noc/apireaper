#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

def print_out(code)
  case code
  when 200, 201, 202
    "Response code is valid: #{code}\n" \
    "Response body is valid\n" \
    "All checks passed\n"
  end
end

describe APIReaper::Requester do # rubocop:disable Metrics/BlockLength
  opts_auth = '-u user -p passwd'
  opts_wauth = '-u wrong -p passwd'

  %w[delete put].each do |method|
    context "check #{method} http://api.test.yueyehua.net/ -q" do
      it 'tries unimplemented commands.' do
        expect { start(self) }.to raise_error(SystemExit)
      end
    end
  end

  context 'check notexistingcmd http://api.test.yueyehua.net/ -q' do
    it 'tries unimplemented commands.' do
      expect { start(self) }.to raise_error(SystemExit)
    end
  end

  %w[get post].each do |method|
    context "check #{method} http://api.test.yueyehua.net/" do
      it 'requests an API without extra arguments.' do
        expect { start(self) }.to output(print_out(200)).to_stdout
      end
    end
  end

  context "check post http://api.user.yueyehua.net/ #{opts_auth}" do
    it 'tries a user and a password.' do
      expect { start(self) }.to output(print_out(200)).to_stdout
    end
  end

  context "check post http://api.user.yueyehua.net/ #{opts_wauth}" do
    it 'tries a wrong user and a password.' do
      expect { start(self) }.to raise_error(SystemExit)
    end
  end

  context 'check post http://api.test.yueyehua.net/ -h k:v' do
    it 'requests an API with headers.' do
      expect { start(self) }.to output(print_out(201)).to_stdout
    end
  end

  context 'check post http://api.test.yueyehua.net/ -d 3wfk=3wfv' do
    it 'requests an API with www_form type datas.' do
      expect { start(self) }.to output(print_out(202)).to_stdout
    end
  end

  opts202 = '-d "{\"jsonk\":\"jsonv\"}" -h Content-type:application/json'
  context "check post http://api.test.yueyehua.net/ #{opts202}" do
    it 'requests an API with json type datas.' do
      expect { start(self) }.to output(print_out(203)).to_stdout
    end
  end

  context 'check post http://api.test.yueyehua.net/ -d k@v&=l:w' do
    it 'requests an API with wrong type of datas.' do
      expect { start(self) }.to raise_error(SystemExit)
    end
  end

  context 'check get https://api.test.yueyehua.net/ -k' do
    it 'requests an API via https using insecure mode.' do
      expect { start(self) }.to output(print_out(200)).to_stdout
    end
  end
end
