#
# Copyright (c) 2017 Richard Delaplace, Vente-Privee.Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

def read_file(file)
  dir = File.dirname(__FILE__)
  File.read(File.join(dir, '..', '..', 'files', file))
end

RSpec.configure do |config| # rubocop:disable Metrics/BlockLength
  config.before(:each) do # rubocop:disable Metrics/BlockLength
    # requests an API without extra arguments
    stub_request(:post, 'http://api.test.yueyehua.net/')
      .to_return('status' => 200, 'body' => '{}', 'headers' => {})
    stub_request(:get, 'http://api.test.yueyehua.net/')
      .to_return('status' => 200, 'body' => '{}', 'headers' => {})
    # request with good credentials
    stub_request(:post, 'http://api.user.yueyehua.net/')
      .with(basic_auth: %w[user passwd])
      .to_return('status' => 200, 'body' => '{}', 'headers' => {})
    # request with wrong credentials
    stub_request(:post, 'http://api.user.yueyehua.net/')
      .with(basic_auth: %w[wrong passwd])
      .to_return('status' => 401, 'body' => '{}', 'headers' => {})
    # requests an API with headers
    stub_request(:post, 'http://api.test.yueyehua.net/')
      .with('headers' => { 'k' => 'v' })
      .to_return('status' => 201, 'body' => '{}', 'headers' => {})
    # requests an API with www_form type datas
    stub_request(:post, 'http://api.test.yueyehua.net/')
      .with('body' => { '3wfk' => '3wfv' })
      .to_return('status' => 202, 'body' => '{}', 'headers' => {})
    # requests an API with json type datas
    stub_request(:post, 'http://api.test.yueyehua.net/')
      .with('body' => { 'jsonk' => 'jsonv' })
      .to_return('status' => 203, 'body' => '{}', 'headers' => {})
    # requests an API and test the response body
    stub_request(:post, 'http://api.test.yueyehua.net/')
      .with('body' => { 'k' => 'v' }, 'headers' => { 'k' => 'v' })
      .to_return('status' => 204, 'body' => read_file('body'), 'headers' => {})
    # requests an API and test the response body with wrong schema file
    stub_request(:post, 'http://api.fail.yueyehua.net/')
      .with('body' => { 'k' => 'v' }, 'headers' => { 'k' => 'v' })
      .to_return('status' => 205, 'body' => read_file('fail'), 'headers' => {})
    # requests an API and receive 404 status code
    stub_request(:post, 'http://api.fail.yueyehua.net/')
      .with('body' => { 'l' => 'w' }, 'headers' => { 'l' => 'w' })
      .to_return('status' => 404, 'body' => '{}', 'headers' => {})
    # requests an API via https using insecure mode
    stub_request(:get, 'https://api.test.yueyehua.net/')
      .to_return('status' => 200, 'body' => '{}', 'headers' => {})
  end
end
